"use strict;"

var MuninBackendPlugin_version = {"major": 0, "minor": 1};
var MuninBackendPlugin_id = "munin";
var MuninBackendPlugin_label = "Munin";
var MuninBackendPlugin_capabilities = [];

class MuninBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
