"use strict;"

var RepositoriesPlugin_version = {"major": 0, "minor": 1};
var RepositoriesPlugin_id = "repositories";
var RepositoriesPlugin_label = "Repositories";
var RepositoriesPlugin_capabilities = [];

class RepositoriesPlugin extends BasePlugin {

	constructor() {
		super();
		this.log("RepositoriesPlugin()");
	}
}
