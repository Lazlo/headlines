"use strict;"

var InstagramBackendPlugin_version = {"major": 0, "minor": 1};
var InstagramBackendPlugin_id = "instagram";
var InstagramBackendPlugin_label = "Instagram";
var InstagramBackendPlugin_capabilities = [];

class InstagramBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
