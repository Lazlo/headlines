"use strict;"

var RsyslogBackendPlugin_version = {"major": 0, "minor": 1};
var RsyslogBackendPlugin_id = "rsyslog";
var RsyslogBackendPlugin_label = "rsyslog";
var RsyslogBackendPlugin_capabilities = [];

class RsyslogBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
