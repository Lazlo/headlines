"use strict;"

var GoogleAnalyticsBackendPlugin_version = {"major": 0, "minor": 1};
var GoogleAnalyticsBackendPlugin_id = "google-analytics";
var GoogleAnalyticsBackendPlugin_label = "Google Analytics";
var GoogleAnalyticsBackendPlugin_capabilities = [];

class GoogleAnalyticsBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
