"use strict;"

var TracBackendPlugin_version = {"major": 0, "minor": 1};
var TracBackendPlugin_id = "trac";
var TracBackendPlugin_label = "trac";
var TracBackendPlugin_capabilities = ["repository-source", "issue-source"];

class TracBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
