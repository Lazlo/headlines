"use strict;"

var SoundCloudBackendPlugin_version = {"major": 0, "minor": 1};
var SoundCloudBackendPlugin_id = "soundcloud";
var SoundCloudBackendPlugin_label = "SoundCloud";
var SoundCloudBackendPlugin_capabilities = [];

class SoundCloudBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
