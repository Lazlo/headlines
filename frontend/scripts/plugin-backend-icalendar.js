"use strict;"

var ICalendarBackendPlugin_version = {"major": 0, "minor": 1};
var ICalendarBackendPlugin_id = "icalendar";
var ICalendarBackendPlugin_label = "iCalendar";
var ICalendarBackendPlugin_capabilities = [];

class ICalendarBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
