"use strict;"

var SlackBackendPlugin_version = {"major": 0, "minor": 1};
var SlackBackendPlugin_id = "slack";
var SlackBackendPlugin_label = "Slack";
var SlackBackendPlugin_capabilities = [];

class SlackBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
