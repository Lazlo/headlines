"use strict;"

var RequestTrackerBackendPlugin_version = {"major": 0, "minor": 1};
var RequestTrackerBackendPlugin_id = "rt";
var RequestTrackerBackendPlugin_label = "Request Tracker";
var RequestTrackerBackendPlugin_capabilities = [];

class RequestTrackerBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
