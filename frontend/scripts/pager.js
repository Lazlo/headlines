"use strict;"

function Pager(currentPageNumTagId, maxPageNumTagId) {
	this.currentPageNumTagId	= null;
	this.maxPageNumTagId		= null;
	this.currentPageNumTag		= null;
	this.maxPageNumTag		= null;

	this.currentPageNumTagId	= currentPageNumTagId;
	this.maxPageNumTagId		= maxPageNumTagId;
	this.currentPageNumTag		= document.getElementById(this.currentPageNumTagId);
	this.maxPageNumTag		= document.getElementById(this.maxPageNumTagId);
}

Pager.prototype.setup = function(currentPage, maxPages) {
	this.currentPageNumTag.innerHTML	= currentPage;
	this.maxPageNumTag.innerHTML		= maxPages;
}

Pager.prototype.update = function(currentPage) {
	this.currentPageNumTag.innerHTML	= currentPage;
}
