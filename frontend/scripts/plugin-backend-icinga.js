"use strict;"

var IcingaBackendPlugin_version = {"major": 0, "minor": 1};
var IcingaBackendPlugin_id = "icinga";
var IcingaBackendPlugin_label = "Icinga";
var IcingaBackendPlugin_capabilities = [];

class IcingaBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
