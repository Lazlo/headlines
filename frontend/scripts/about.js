class AboutDialog {

	_populateAboutDialog(pluginRegistry) {
		var tagId = "plugin-registry-plugins-available";
		var target = document.getElementById(tagId);

		/* Get a list of plugins */

		var pluginList = pluginRegistry.getPluginsEndingWith("");
		var list = pluginRegistry._sortPluginListByName(pluginList);
		var i;

		var tableHtml = "";

		tableHtml += "<table border=\"1\" width=\"100%\">";
		tableHtml += "<tr>";
		tableHtml += "<th>Plugin</th>";
		tableHtml += "<th>Version</th>";
		tableHtml += "<th>Type</th>";
		tableHtml += "</tr>";

		for (i = 0; i < list.length; i++) {
			var plugin = list[i];

			tableHtml += "<tr>";
			tableHtml += "<td>" + plugin.getLabel() + "</td>";
			tableHtml += "<td align=\"center\">" + plugin.getVersion() + "</td>";
			tableHtml += "<td>" + Object.getPrototypeOf(plugin).name + "</td>";
			tableHtml += "</tr>";
		}

		tableHtml += "</table";

		target.innerHTML = "";
		target.innerHTML += tableHtml;


		target.innerHTML += "<table>";
	}

	show() {
		var uiDialogTagId = "slideshow-about";
		var uiDialogTag = document.getElementById(uiDialogTagId);

		/* FIXME Using the name of a global variable is just wrong.
		 * Have a reference passed to the registry passed to this object after creation. */

		this._populateAboutDialog(pluginRegistry);
		showElement(uiDialogTag);
	}

	hide() {
		var uiDialogTagId = "slideshow-about";
		var uiDialogTag = document.getElementById(uiDialogTagId);
		hideElement(uiDialogTag);
	}
}
