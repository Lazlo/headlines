"use strict;"

var OdooBackendPlugin_version = {"major": 0, "minor": 1};
var OdooBackendPlugin_id = "odoo";
var OdooBackendPlugin_label = "odoo";
var OdooBackendPlugin_capabilities = [];

class OdooBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
