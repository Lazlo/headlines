"use strict;"

var FacebookBackendPlugin_version = {"major": 0, "minor": 1};
var FacebookBackendPlugin_id = "facebook";
var FacebookBackendPlugin_label = "Facebook";
var FacebookBackendPlugin_capabilities = [];

class FacebookBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
