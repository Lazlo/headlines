"use strict;"

var SlideshowPlugin_version = {"major": 0, "minor": 1};
var SlideshowPlugin_id = "slideshow";
var SlideshowPlugin_label = "Slideshow";
var SlideshowPlugin_capabilities = [];

class SlideshowPlugin extends BasePlugin {

	constructor() {
		super();
		this.repeat = false;
		this.interval =  null;
		this.slideDelayMs = null;
		this.pluginWidgetList = null;
		this.slideIndex = null;
		this.currentSlide = null;
		this.pluginTagId = {
			'title'		: "plugin-title",
			'content'	: "plugin-content",
			'settings'	: "plugin-settings",
		};
		this.ui = {
			'navigation'		: {"tagId": null, "tag": null},
			'currentSlideContent'	: {"tagId": null, "tag": null},
			'controls': {
				'pause'		: {"tagId": null, "tag": null},
				'resume'	: {"tagId": null, "tag": null},
			},
		};
		this.pager = null;
		this.settings = new Settings();
		//this.settings.debug = true;
		this.settings._populateBackendsAvailableSelect();

		this.about = new AboutDialog();
	}

	_log(msg) {
		if (!this.debug)
			return;
		var prefix = "Slidshow.";
		console.log(prefix + msg);
	}

	restoreConfigs() {
		this._log("restoreConfigs()");
		this.settings.restoreConfigs();
	}

	updateTitles() {
		this._log("updateTitles()");
		var target = this.ui.navigation.tag;
		var list = this.pluginWidgetList;
		var slideIndex = this.slideIndex;
		var i;
		target.innerHTML = "";
		for (i = 0; i < list.length; i++) {
			var p = list[i]["tag"];
			var title = p.getElementsByClassName(this.pluginTagId.title)[0];
			var titleHtml;

			/* FIXME Using the name of the of the variable the slideshow objects
			 * resides in is just wrong! We need to find a different way.
			 * We could pass the name of the variable to the object on creation.
			 */

			titleHtml = "<a ";
			titleHtml += "class\"slide-title\" ";
			titleHtml += "href=\"#\" onclick=\"slideshow.pauseAndGoToSlide(" + i + ");\">";
			titleHtml += title.innerHTML;
			titleHtml += "</a>";
			if (slideIndex == i) {
				titleHtml = "<strong>" + titleHtml + "</strong>";
			}
			titleHtml = "<span class=\"slide-title\">" + titleHtml + "</span>";

			target.innerHTML += titleHtml;
		}
	}

	renderSlide(slideIndex) {
		this._log("renderSlide()");
		this.slideIndex = slideIndex;
		this.currentSlide = this.pluginWidgetList[slideIndex]["tag"];

		/* FIXME Rather only hide the slide that was shown last */

		/* Replacement for hideAllPluginWidgets() */

		var list = this.pluginWidgetList;
		var i;
		for (i = 0; i < list.length; i++) {
			var tag = list[i]["tag"];
			hideElement(tag);
		}
		showElement(this.currentSlide);

		/* Setup the slide titles */

		this.updateTitles();

		/* Update the pager */

		this.pager.update(this.slideIndex + 1);
	}

	setup(pluginWidgetList, slideDelaySec, repeat) {
		this._log("setup()");
		this.slideDelayMs = slideDelaySec * 1000;
		this.pluginWidgetList = pluginWidgetList;
		this.repeat = repeat;
		this.slideIndex = 0;

		this.ui.navigation.tagId	= "slideshow-slide-title-list";
		this.ui.currentSlideContent.tagId	= "current-slide-content";
		this.ui.controls.pause.tagId	= "slideshow-controls-pause";
		this.ui.controls.resume.tagId	= "slideshow-controls-resume";

		this.ui.navigation.tag		= document.getElementById(this.ui.navigation.tagId);
		this.ui.currentSlideContent.tag	= document.getElementById(this.ui.currentSlideContent.tagId);
		this.ui.controls.pause.tag	= document.getElementById(this.ui.controls.pause.tagId);
		this.ui.controls.resume.tag	= document.getElementById(this.ui.controls.resume.tagId);

		/* Replacement for hideAllPluginWidgets() */

		var list = this.pluginWidgetList;
		var i;
		for (i = 0; i < list.length; i++) {
			var tag = list[i]["tag"];
			hideElement(tag);
		}

		/* Create the pager object and set it up */

		this.pager = new Pager("current-slide-index", "max-slides");
		this.pager.setup(this.slideIndex + 1, this.pluginWidgetList.length);

		/* Render first slide with titles of all slides and pager */

		this.renderSlide(this.slideIndex);

		/* Attach onclick handlers to slideshow controls */
		var pauseBtn = this.ui.controls.pause.tag;
		var resumeBtn = this.ui.controls.resume.tag;

		pauseBtn.onclick = this.pause.bind(this);
		resumeBtn.onclick = this.resume.bind(this);

		/* Attach onclick handlers on all save-slide-settings buttons */

		var className = "save-slide-settings";
		var list = document.getElementsByClassName(className);
		var i;
		for (i = 0; i < list.length; i++) {
			var tag = list[i];
			tag.onclick = this.saveSlideSettings.bind(this);
		}
	}

	updateControls(paused) {
		this._log("updateControls()");
		var resumeImg = this.ui.controls.resume.tag;
		var pauseImg = this.ui.controls.pause.tag;
		setElementVisible(resumeImg, paused ? 1 : 0);
		setElementVisible(pauseImg, paused ? 0 : 1);
	}

	resume() {
		this._log("resume()");
		this.interval = window.setInterval(this.updateSlideshow.bind(this), this.slideDelayMs);
		this.updateControls(0);
	}

	pause() {
		this._log("pause()");
		window.clearInterval(this.interval);
		this.updateControls(1);
	}

	pauseAndGoToSlide(slideIndex) {
		this._log("pauseAndGoToSlide()");
		this.pause();
		this.renderSlide(slideIndex);
	}

	run() {
		this._log("run()");
		this.resume();
	}

	isLastSlide() {
		this._log("isLastSlide()");
		return (this.slideIndex == this.pluginWidgetList.length - 1) ? true : false;
	}

	nextSlide() {
		this._log("nextSlide()");
		this.slideIndex = (this.isLastSlide() == false) ? this.slideIndex + 1 : 0;
	}

	/*
	 * Slide Settings Methods
	 */

	setSlideSettingsVisible(visible) {
		this._log("setSlideSettingsVisible()");
		var plugin = this.currentSlide;

		/* Not only should the plugin settings dialog become visible
		 * but its contents should also be updated (e.g. so new backend
		 * configurations will become visible without having to reload the page) */
		var pluginObj = this.pluginWidgetList[this.slideIndex]["pluginObj"];
		pluginObj.setupSettings(this.settings);

		var settings = plugin.getElementsByClassName(this.pluginTagId.settings)[0];
		setElementVisible(settings, visible);
	}

	showSlideSettings() {
		this._log("showSlideSettings()");
		this.setSlideSettingsVisible(true);
	}

	hideSlideSettings() {
		this._log("hideSlideSettings()");
		this.setSlideSettingsVisible(false);
	}

	saveSlideSettings() {
		this._log("saveSlideSettings()");
		this.hideSlideSettings();

		/* Call the save method of the current plugin widget displayed */

		var plugin = this.pluginWidgetList[this.slideIndex]["pluginObj"];
		var saveSettingsFunc = plugin.saveSettings;
		if (!saveSettingsFunc)
			return;
		plugin.saveSettings();
	}

	dismissSlideSettings() {
		this._log("dismissSlideSettings()");
		this.hideSlideSettings();
	}

	updateSlideshow() {
		this._log("update()");
		if (this.repeat == false && this.isLastSlide()) {
			this.pause();
			return;
		}
		this.nextSlide();
		this.renderSlide(this.slideIndex);
	}

	/*
	 * Slide Configuration Methods
	 */

	_setupSlideList() {
		var tagId = "slideshow-slide-list";
		var target = document.getElementById(tagId);
		target.innerHTML = null;

		var list = this.pluginWidgetList;
		var i;
		for (i = 0; i < list.length; i++) {
			var pluginObj = list[i]["pluginObj"];
			var classObj = pluginObj.constructor;
			var div = document.createElement("div");
			var att = document.createAttribute("class");
			att.value = "slideshow-slide-list-entry";
			div.setAttributeNode(att);
			div.innerHTML = classObj.getLabel();
			target.appendChild(div);
		}
	}

	_setupSlideDelayInput() {
		var inputTagId = "plugin-slideshow-slide-delay-seconds";
		var inputTag = document.getElementById(inputTagId);

		var defaultSlideDelaySeconds = 1;
		var slideDelaySeconds = defaultSlideDelaySeconds;
		if (this.slideDelayMs) {
			slideDelaySeconds = this.slideDelayMs / 1000;
		}
		inputTag.value = slideDelaySeconds;
	}

	showConfig() {
		var uiDialogTagId = "slideshow-config";
		var uiDialogTag = document.getElementById(uiDialogTagId);

		/* TODO Before we show the config dialog, we need to populate
		 * the list of slides in the UI. */
		this._setupSlideList();
		this._setupSlideDelayInput();

		showElement(uiDialogTag);
	}

	hideConfig() {
		var uiDialogTagId = "slideshow-config";
		var uiDialogTag = document.getElementById(uiDialogTagId);
		hideElement(uiDialogTag);
	}
}
