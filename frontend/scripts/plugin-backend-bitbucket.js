"use strict;"

var BitbucketBackendPlugin_version = {"major": 0, "minor": 1};
var BitbucketBackendPlugin_id = "bitbucket";
var BitbucketBackendPlugin_label = "Bitbucket";
var BitbucketBackendPlugin_capabilities = [];

class BitbucketBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
