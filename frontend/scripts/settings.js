"use strict;"

class Settings {

	constructor() {
		this.debug = false;
		this.ui = {
			'dialog'		: {"tagId": null, "tag": null},
			'backendsAvailable'	: {"tagId": null, "tag": null},
			'configuredNone'	: {"tagId": null, "tag": null},
			'configuredBackends'	: {"tagId": null, "tag": null},
			'backendAdd'		: {"tagId": null, "tag": null},
			'downloadSettings'	: {"tagId": null, "tag": null},
		};

		/* FIXME Passing the name of a global variable is evil! */

		this.backends = this._getListOfBackendsFromRegistry(pluginRegistry);

		this.configuredBackends = [];

		this.ui.dialog.tagId		= "slideshow-settings";
		this.ui.backendsAvailable.tagId	= "slideshow-backends-available";
		this.ui.configuredNone.tagId	= "slideshow-backends-configured-none";
		this.ui.configuredBackends.tagId	= "slideshow-backends-configured";
		this.ui.backendAdd.tagId	= "slideshow-backend-add";
		this.ui.downloadSettings.tagId	= "slideshow-download-configured-backends";
		this.ui.dialog.tag		= document.getElementById(this.ui.dialog.tagId);
		this.ui.backendsAvailable.tag	= document.getElementById(this.ui.backendsAvailable.tagId);
		this.ui.configuredNone.tag	= document.getElementById(this.ui.configuredNone.tagId);
		this.ui.configuredBackends.tag	= document.getElementById(this.ui.configuredBackends.tagId);
		this.ui.backendAdd.tag		= document.getElementById(this.ui.backendAdd.tagId);
		this.ui.downloadSettings.tag	= document.getElementById(this.ui.downloadSettings.tagId);
	}

	_log(msg) {
		if (!this.debug)
			return;
		console.log("Settings." + msg);
	}

	_restoreConfiguredBackendsFromLocalStorage() {
		this._log("_restoreConfiguredBackendsFromLocalStorage()");
		var c = new Configuration();
		var restoredConfiguredBackends = c.load("slideshow-configuredBackends")
		if (!restoredConfiguredBackends)
			return;
		this.configuredBackends = restoredConfiguredBackends;
	}

	_saveConfiguredBackendsToLocalStorage() {
		this._log("_saveConfiguredBackendsToLocalStorage()");
		var c = new Configuration();
		c.save("slideshow-configuredBackends", this.configuredBackends);
	}

	restoreConfigs() {
		this._log("restoreConfigs()");
		this._restoreConfiguredBackendsFromLocalStorage();

		/* If there are entries in the list of configured backends,
		 * hide the "no backend configured" entry. */

		var list = this.configuredBackends;
		if (list.length > 0) {
			var noConfig = document.getElementById(this.ui.configuredNone.tagId);
			noConfig.style.display = "none";
		}

		/* Create the UI elements for the configurations found */

		var i;
		var config;
		for (i = 0; i < list.length; i++) {
			config = list[i];
			this._addUiEntryForConfiguredBackend(config["id"], config["name"], config["backend"]);
		}
	}

	_getListOfBackendsFromRegistry(registry) {
		var list = registry._sortPluginListByName(registry.getPluginsEndingWith("BackendPlugin"))
		var i;
		var backends = [
			{"id": null, "label": null},
		];
		for (i = 0; i < list.length; i++) {
			var plugin = list[i];
			var entry = {"id": null, "label": null};
			entry["id"] = plugin.getId();
			entry["label"] = plugin.getLabel();
			backends.push(entry);
		}
		return backends;
	}

	_createSelectOption(optValue, optLabel) {
		var opt = document.createElement("option");
		var att = document.createAttribute("value");
		att.value = optValue;
		opt.setAttributeNode(att);
		opt.innerHTML = optLabel;
		return opt;
	}

	_populateBackendsAvailableSelect() {
		var select = this.ui.backendsAvailable.tag;
		var list = this.backends;
		var i;
		for (i = 0; i < list.length; i++) {
			var backend = list[i];
			var opt = this._createSelectOption(backend["id"], backend["label"]);
			select.appendChild(opt);
		}
	}

	_getAllBackendConfigDialogs() {
		var className = "slideshow-backend-config-dialog";
		var list = document.getElementsByClassName(className);
		return list;
	}

	_hideAllBackendConfigDialogs() {
		this._log("_hideAllBackendConfigDialogs()");
		var list = this._getAllBackendConfigDialogs();
		var i;
		var dialog;
		for (i = 0; i < list.length; i++) {
			dialog = list[i];
			dialog.style.display = "none";
		}
	}

	_showBackendConfigDialog(backendName) {
		this._log("_showBackendConfigDialog()");
		var expectedId = "slideshow-backend-config-" + backendName;
		var list = this._getAllBackendConfigDialogs();
		var i;
		var dialog;
		for (i = 0; i < list.length; i++) {
			dialog = list[i];
			if (dialog.id != expectedId)
				continue;
			dialog.style.display = "inline-block";
		}

		/* Make sure to clear the values from the dialog */

		var classObj = pluginRegistry.getPluginById(backendName);
		if (!classObj) {
			console.error("Failed to get plugin for " + backendName);
			return;
		}
		var b = new classObj();
		b.settings.resetFormFields();
		/* Register event listeners for backend config dialog specific inputs */
		b.settings.addEventListenersToForm();
	}

	_setSelectedBackend(index) {
		this._log("_setSelectedBackend()");
		var selectTag = this.ui.backendsAvailable.tag;
		selectTag.selectedIndex = index;
	}

	_getSelectedBackend() {
		this._log("_getSelectedBackend()");
		var selectTag = this.ui.backendsAvailable.tag;
		var selection = selectTag.options[selectTag.selectedIndex].value;
		return selection;
	}

	_onSelectedBackendChanged() {
		this._log("_onSelectedBackendChanged()");
		this._hideAllBackendConfigDialogs();
		var selection = this._getSelectedBackend();
		this._log("selected " + selection);
		this._showBackendConfigDialog(selection);
	}

	_setVisible(visible) {
		this._log("_setVisible()");
		setElementVisible(this.ui.dialog.tag, visible);
	}

	_getConfiguredBackendIndexByConfigId(configId) {
		this._log("_getConfiguredBackendIndexByConfigId()");
		var list = this.configuredBackends;
		var i;
		var configIndex = null;
		for (i = 0; i < list.length; i++) {
			if (configId != list[i]["id"]) {
				continue;
			}
			configIndex = i;
			break;
		}
		return configIndex;
	}

	_getConfiguredBackendByConfigIndex(configIndex) {
		this._log("_getConfiguredBackendByConfigIndex()");
		var config = this.configuredBackends[configIndex];
		return config;
	}

	_getConfiguredBackendByConfigId(configId) {
		this._log("_getConfiguredBackendByConfigId()");
		var configIndex = this._getConfiguredBackendIndexByConfigId(configId);
		var config = null;
		if (configIndex == null) {
			return config;
		}
		config = this._getConfiguredBackendByConfigIndex(configIndex);
		return config;
	}

	showConfiguredBackend(entry) {
		this._log("showConfiguredBackend()");
		/* look up the entry we want to display */
		var configId = entry.id.replace("configured-backend-entry-", "");
		var config = this._getConfiguredBackendByConfigId(configId);
		if (!config) {
			console.error("ERROR: failed to get config by configId " + configId);
			return;
		}
		/* look up the backend type */
		var backendName = config["backend"];

		/* Reset the 'backends available section */
		this._setSelectedBackend(0);

		/* display the respective backend config dialog */
		this._hideAllBackendConfigDialogs();
		this._showBackendConfigDialog(backendName);

		/* Ask the plugin for the tagIds of its form fields */

		/* Resolve backendName into the class name of the
		 * plugin to create a object of that type. */

		var backendClassObj = pluginRegistry.getPluginById(backendName);

		var b = new backendClassObj();
		if (!b.settings || !b.settings.getSchema) {
			console.error("Failed getting schema from " + backendName);
			return;
		}
		var schema = b.settings.getSchema();

		/* Now look at the existing configuration and place the values
		 * back into the settings form. */

		var i;
		for (i = 0; i < schema.length; i++) {
			var mapping = schema[i];
			var settingsKey = mapping["settingsKey"];
			var configValue = config[settingsKey];

			/* Check if there is a value in the existing configuration */
			if (!configValue) {
				continue;
			}

			var inputTagId = mapping["tagId"];
			/* Get the form field and inser the value from the config */
			var input = document.getElementById(inputTagId);
			input.value = configValue;
		}
	}

	removeConfiguredBackend(configId) {
		this._log("removeConfiguredBackend()");
		var configIndex = this._getConfiguredBackendIndexByConfigId(configId);
		if (configIndex == null) {
			console.error("Failed to get configIndex by configId " + configId);
			return;
		}
		var config = this.configuredBackends[configIndex];
		if (!config) {
			console.error("ERROR: Failed to get config by configIndex " + configIndex);
			return;
		}
		var backendName = config["backendName"];

		/* TODO If the current settings dialog belongs to that config
		 * we need to hide the dialog. */

		/* First remove the UI element */

		var uiEntryTagId = "configured-backend-entry-" + configId;
		var uiEntry = document.getElementById(uiEntryTagId);

		uiEntry.parentElement.removeChild(uiEntry);

		/* Remove the entry from the array */
		this.configuredBackends.splice(configIndex, 1);

		/* If this was the last entry, show the 'no configs' message again */

		if (this.configuredBackends.length == 0) {
			var noConfig = document.getElementById(this.ui.configuredNone.tagId);
			noConfig.style.display = "block";
		}
	}

	_addUiEntryForConfiguredBackend(configId, configName, backendName) {
		this._log("_addUiEntryForConfiguredBackend()");
		/* TODO Construct element programatically */
		/*
		var divTag = document.createElement("div");
		var idAtt = document.createAttribute("id");
		var classAtt = document.createAttribute("class");
		idAtt.value = "configured-backend-entry-" + configId;
		classAtt.value = "slideshow-backends-configured-list-entry";

		divTag.setAttributeNode(idAtt);
		divTag.setAttributeNode(classAtt);

		var imgTag = document.createElement("img");
		*/

		/* FIXME Using the name of the variable the object is stored in is
		 * just wrong! We need to change that in such way that the object
		 * knows the name of the variable it resides in or find another way. */

		var divClass	= "slideshow-backends-configured-list-entry";
		var divId	= "configured-backend-entry-" + configId;

		var entryHtml;
		entryHtml = "<div class=\"" + divClass + "\" ";
		entryHtml += "id=\"" + divId + "\" ";
		entryHtml += "onclick=\"slideshow.settings.showConfiguredBackend(this);\">";
		entryHtml += "<img src=\"images/logo-" + backendName + ".svg\" height=\"10%\" />";
		entryHtml += configName;
		entryHtml += "<span class=\"remove\" ";
		entryHtml += "onclick=\"slideshow.settings.removeConfiguredBackend(" + configId + ");\">-</span>";
		entryHtml += "</div>";

		/* Get the element that represents the list of configured backends
		 * and add the new element of top of existing configurations inside the element. */

		var configuredList = this.ui.configuredBackends.tag;
		var existingContent = configuredList.innerHTML;
		configuredList.innerHTML = entryHtml + existingContent;
	}

	_addConfiguredBackend(backendName, configName) {
		this._log("_addConfiguredBackend()");

		/* If this is the first configured backend, hide the "no backend configured"
		 * entry. */

		if (this.configuredBackends.length == 0) {
			var noConfig = document.getElementById(this.ui.configuredNone.tagId);
			noConfig.style.display = "none";
		}

		/* Now, find a free config id */
		var i;
		var configId = 1;
		for (i = 0; i < this.configuredBackends.length; i++) {
			/* If id is used, increment it and go to the next
			 * entry. Check if we need to increment again. */
			if (configId == this.configuredBackends[i]["id"]) {
				configId++
			}
		}

		var config = {};

		/* Call backend and ask for the form fields that we need to save */

		var classObj = pluginRegistry.getPluginById(backendName);

		var b = new classObj();
		if (!b.settings || !b.settings.createConfigFromFormFields) {
			console.error("Failed to create config from form fields.");
			return;
		}
		config = b.settings.createConfigFromFormFields();

		/* Add the default fields all configs have in common */

		config["id"]		= configId;
		config["backend"]	= backendName;
		config["name"]		= configName;

		console.log(config);

		this.configuredBackends.push(config);

		this._addUiEntryForConfiguredBackend(configId, configName, backendName);
	}

	_onAddConfigClick() {
		this._log("_onAddConfigClick()");
		var backendName = this._getSelectedBackend();

		if (backendName == "") {
			console.log("invalid backend!");
			return;
		}

		var nameInputTagId = "slideshow-backend-config-" + backendName + "-name";
		var nameInputTag = document.getElementById(nameInputTagId);
		var configName = nameInputTag.value;

		if (configName == "") {
			this._log("name must not be empty!");
			return;
		}

		this._addConfiguredBackend(backendName, configName);

		/* Clear the dialog input fields */

		nameInputTag.value = null;

		/* Select the empty option in the backend select input */

		this._setSelectedBackend(0);
		this._onSelectedBackendChanged();
	}

	show() {
		this._log("show()");
		this._setVisible(true);
		this._hideAllBackendConfigDialogs();

		var selectTag = this.ui.backendsAvailable.tag;
		selectTag.onchange = this._onSelectedBackendChanged.bind(this);

		/* Add onclick handler to "+" button that will add the configured
		 * backend to the list of configured backends. */

		var addTag = this.ui.backendAdd.tag;
		addTag.onclick = this._onAddConfigClick.bind(this);
	}

	_hide() {
		this._log("_hide()");
		this._setVisible(false);
	}

	save() {
		this._log("save()");
		this._hide();
		this._saveConfiguredBackendsToLocalStorage();
	}

	dismiss() {
		this._log("dismiss()");
		this._hide();
	}

	download() {
		this._log("download()");
		var aTag = this.ui.downloadSettings.tag;
		var fileName = "settings.txt";
		var fileContents = JSON.stringify(this.configuredBackends, null, "\t") + "\r\n";
		downloadTextAsFile(aTag, fileName, fileContents);
	}
}
