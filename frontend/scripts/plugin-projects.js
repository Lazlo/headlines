"use strict;"

var ProjectsPlugin_version = {"major": 0, "minor": 2};
var ProjectsPlugin_id = "projects";
var ProjectsPlugin_label = "Projects";
var ProjectsPlugin_capabilities = [];

class ProjectsPlugin extends BasePlugin {

	constructor() {
		super();
		//this.debug = true;

		this.log("ProjectsPlugin()");

		this.refToSettingsPluginObj = null;
		/* FIXME The item name should reflect the plugin instance */
		this.localStorageSettingsItemKey = "plugin-projects-1-settings";
		this.settings = {
			'backendConfigId': null,
			'refreshIntervalSeconds': null,
		};

		this.interval = null;

		this.ui = {
			'projList'				: {"tagId": null, "tag": null},
			'projListTimestamp'			: {"tagId": null, "tag": null},
			'supportedBackends'			: {"tagId": null, "tag": null},
			'settingsBackendConfig'			: {"tagId": null, "tag": null},
			'settingsRefreshIntervalSeconds'	: {"tagId": null, "tag": null},
			'settingsExportDump'			: {"tagId": null, "tag": null},
		};
		this.ui.projList.tagId				= "project-list";
		this.ui.projListTimestamp.tagId			= "project-list-last-updated";
		this.ui.supportedBackends.tagId			= "plugin-projects-supported-backends";
		this.ui.settingsBackendConfig.tagId		= "plugin-projects-backend-config";
		this.ui.settingsRefreshIntervalSeconds.tagId	= "project-list-refresh-interval-seconds";
		this.ui.settingsExportDump.tagId		= "plugin-projects-settings-export-dump";
		this.ui.projList.tag				= document.getElementById(this.ui.projList.tagId);
		this.ui.projListTimestamp.tag			= document.getElementById(this.ui.projListTimestamp.tagId);
		this.ui.supportedBackends.tag			= document.getElementById(this.ui.supportedBackends.tagId);
		this.ui.settingsBackendConfig.tag		= document.getElementById(this.ui.settingsBackendConfig.tagId);
		this.ui.settingsRefreshIntervalSeconds.tag	= document.getElementById(this.ui.settingsRefreshIntervalSeconds.tagId);
		this.ui.settingsExportDump.tag			= document.getElementById(this.ui.settingsExportDump.tagId);
	}

	_log(msg) {
		if (!this.debug)
			return;
		var prefix = "ProjectsPlugin.";
		console.log(prefix + msg);
	}

	_restoreSettingsFromLocalStorage() {
		this._log("_restoreSettingsFromLocalStorage()");
		var itemKey = this.localStorageSettingsItemKey;
		var c = new Configuration();
		var itemValue = c.load(itemKey);
		if (!itemValue)
			return;
		this.settings = itemValue;
	}

	_saveSettingsToLocalStorage() {
		this._log("_saveSettingsToLocalStorage()");
		var itemKey = this.localStorageSettingsItemKey;
		var c = new Configuration();
		c.save(itemKey, this.settings);
	}

	_onSelectedBackendConfigChanged() {
		this._log("_onSelectedBackendConfigChanged()");
		var select = this.ui.settingsBackendConfig.tag;

		/* Get the selection */

		var selected = select.options[select.selectedIndex].value;
		var configId = selected.replace("config-id-", "");

		/* Save the selection in the settings attribute of this object */
		this.settings.backendConfigId = configId
	}

	setupSettings(settingsPluginObj) {
		this._log("setupSettings()");

		/*
		 * Part 1 - Check for a existing configuration for this Plugin
		 * in local storage.
		 */

		this._restoreSettingsFromLocalStorage();

		/*
		 * Part 2 - Get a list of available backend configurations
		 * that are compatible with this plugin.
		 *
		 * Also save a pointer to the settings object that manages
		 * the backend configurations.
		 */

		var select = this.ui.settingsBackendConfig.tag;

		/* Save a reference to the settings plugin object */
		this.refToSettingsPluginObj = settingsPluginObj;

		/* Add on change handler */
		select.onchange = this._onSelectedBackendConfigChanged.bind(this);

		/* Add empty option */
		var opt = document.createElement("option");
		select.innerHTML = null;
		select.appendChild(opt);

		var list = settingsPluginObj.configuredBackends
		var i;
		for (i = 0; i < list.length; i++) {
			var opt = document.createElement("option");
			var att = document.createAttribute("value");
			att.value = "config-id-" + list[i]["id"];
			/* If a configuration has been chosen and restored,
			 * mark it selected. */
			if (this.settings.backendConfigId == list[i]["id"]) {
				opt.selected = true;
			}
			opt.setAttributeNode(att);
			opt.innerHTML = "[" + list[i]["backend"] + "] " + list[i]["name"];
			select.appendChild(opt);
		}


		/* Populate the list of supported backends */
		var target = this.ui.supportedBackends.tag;
		target.innerHTML = "";

		var list = pluginRegistry.getPluginsWithCapability("repository-source");
		for (i = 0; i < list.length; i++) {
			target.innerHTML += list[i].getLabel();
			if (i + 1 < list.length)
				target.innerHTML += ", ";
		}

		/* Restore the refresh interval input value */
		var target = this.ui.settingsRefreshIntervalSeconds.tag;
		target.value = this.settings["refreshIntervalSeconds"];

		/* Finally call update */
		this.update();

		/* Set up interval (if not set up yet) */
		if (!this.interval) {
			var refershIntervalSeconds = this.settings["refreshIntervalSeconds"];
			var refershIntervalMs = refershIntervalSeconds * 1000;
			this.interval = window.setInterval(this.update.bind(this), refershIntervalMs);
		}
	}

	update() {
		this._log("update()");
		var projList = this.ui.projList.tag;
		if (this.settings.backendConfigId == null) {
			var errMsg = "The plugin is unconfigured.";
			this.log("ProjectsPlugin.update() - no config");
			projList.innerHTML = "<p>" + errMsg + "</p>";
			return;
		}

		/* Only show this loading message when no list of projects
		 * has been fetched before! */
		var projListTimestamp = this.ui.projListTimestamp.tag;
		if (projListTimestamp.innerHTML == "never") {
			var loadingMsg = "Loading ...";
			projList.innerHTML = "<p>" + loadingMsg + "</p>";
		}

		/* Get the backend configuration object from the settings object. */

		var configId = this.settings["backendConfigId"];
		var config = this.refToSettingsPluginObj._getConfiguredBackendByConfigId(configId);

		if (!config) {
			console.warn("Failed to get config with id " + configId);
			return;
		}

		/* Get the backend that matches the current configuration */

		if (config["backend"] == "gitlab") {
			var gitlab = new GitlabBackendPlugin();
			gitlab.setPrivateToken(config["privateToken"]);
			gitlab.fetchProjects(pluginProjects.populateProjList.bind(this));
		} else if (config["backend"] == "github") {
			var b = new GithubBackendPlugin();
			b.setAccessToken(config["accessToken"]);
			b.fetchProjects(pluginProjects.populateProjList.bind(this));
		}
	}

	/* Currently for gitlab JSON response only */
	populateProjList(responseText) {
		this._log("populateProjList()");

		/* Get the backend name of the current configuration */
		var configId = this.settings["backendConfigId"];
		var config = this.refToSettingsPluginObj._getConfiguredBackendByConfigId(configId);
		var backendName = config["backend"];

		/* FIXME It seems that when we pass a reference of this method
		 * to another function as a callback, the object context (this)
		 * is lost.
		 *
		 * This might be possible to fix by adding another argument to the
		 * function that accepts the callback, to provide the object context.
		 * But I hope there is another way to do it.
		 */

		//this.log("ProjectsPlugin.populateProjList()");
		var projListTag = this.ui.projList.tag;
		var projListStr = "";
		var jsonResponse = JSON.parse(responseText);
		var i;

		projListStr += "<ul>";
		for (i = 0; i < jsonResponse.length; i++) {
			var proj = jsonResponse[i];

			/* GitLab */
			if (backendName == "gitlab") {
				var proj_url		= proj.http_url_to_repo;
				var proj_name		= proj.name;
				var proj_issues_count	= proj.open_issues_count;
				var proj_last_activity	= printTimePassedMostSignificantOnly(getTimePassedSinceNow(new Date(proj.last_activity_at)));
			} else if (backendName == "github") {
				var proj_url		= proj.html_url;
				var proj_name		= proj.name;
				var proj_issues_count	= proj.open_issues_count;
				var proj_last_activity	= proj.updated_at;
			}

			projListStr += "<li>";
			projListStr += "<a href=\"" + proj_url + "\">";
			projListStr += proj_name;
			projListStr += "</a>";
			projListStr += " (Issues: " + proj_issues_count;
			projListStr += " modfied: " + proj_last_activity;
			projListStr += ")";
			projListStr += "</li>";
		}
		projListStr += "</ul>";
		projListTag.innerHTML = projListStr;

		/* Update the 'last updated' time stamp */
		var target = this.ui.projListTimestamp.tag;
		target.innerHTML = new Date();
	}

	/* Settings related methods */

	saveSettings() {
		this._log("saveSettings()");
		var backendCfgSelect = this.ui.settingsBackendConfig.tag;
		var riInput	= this.ui.settingsRefreshIntervalSeconds.tag;

		var selected = backendCfgSelect.options[backendCfgSelect.selectedIndex];

		this.settings.refreshIntervalSeconds	= riInput.value;

		/* Persist settings into localStorage */
		this._saveSettingsToLocalStorage();
	}

	exportSettings() {
		this._log("exportSettings()");
		var textArea = this.ui.settingsExportDump.tag;
		var jsonSettings = JSON.stringify(this.settings, null, "\t");
		textArea.value = jsonSettings;
	}
}
