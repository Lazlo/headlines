"use strict;"

var GithubBackendPlugin_version = {"major": 0, "minor": 1};
var GithubBackendPlugin_id = "github";
var GithubBackendPlugin_label = "GitHub";
var GithubBackendPlugin_capabilities = ["repository-source", "issue-source"];

class GithubBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();

		this.accessToken = null;
		this.settings = new GithubBackendPluginSettings();
	}

	setAccessToken(accessToken) {
		this.log("setAccessToken");
		this.accessToken = accessToken;
	}

	fetchProjects(responseTextHandler) {
		this.log("fetchProjects()");
		var url;
		url = "https://api.github.com/users/lazlo/repos";
		httpGet(url, responseTextHandler);
	}
}

class GithubBackendPluginSettings {

	constructor() {
		this.schema = [];
		this.schema.push({"settingsKey": "name",	"tagId": "slideshow-backend-config-github-name"});
		this.schema.push({"settingsKey": "accessToken",	"tagId": "slideshow-backend-config-github-access-token"});
	}

	_log(msg) {
		var prefix = "GithubBackendPluginSettings.";
		console.log(prefix + msg);
	}

	getSchema() {
		this._log("getSchema()");
		return this.schema;
	}

	addEventListenersToForm() {
		this._log("addEventListenersToForm()");
	}

	resetFormFields() {
		this._log("resetFormFields()");
		var list = this.schema;
		var i;
		for (i = 0; i < list.length; i++) {
			var settingsKey = list[i]["settingsKey"];
			var tagId = list[i]["tagId"];
			var input = document.getElementById(tagId);
			input.value = null;
		}
	}

	/* NOTE Identical to gitlab */
	createConfigFromFormFields() {
		this._log("createConfigFromFormFields()");
		var config = {};
		var i;
		for (i = 0; i < this.schema.length; i++) {
			var settingsKey = this.schema[i]["settingsKey"];
			var tagId = this.schema[i]["tagId"];

			config[settingsKey] = null;
			var input = document.getElementById(tagId);
			config[settingsKey] = input.value;
		}
		return config;
	}
}
