"use strict;"

var NewsPlugin_version = {"major": 0, "minor": 1};
var NewsPlugin_id = "news";
var NewsPlugin_label = "News";
var NewsPlugin_capabilities = [];

class NewsPlugin extends BasePlugin {

	constructor() {
		super();
		this.log("NewsPlugin()");
	}
}
