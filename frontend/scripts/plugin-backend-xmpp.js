"use strict;"

var XmppBackendPlugin_version = {"major": 0, "minor": 1};
var XmppBackendPlugin_id = "xmpp";
var XmppBackendPlugin_label = "XMPP/Jabber";
var XmppBackendPlugin_capabilities = [];

class XmppBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
