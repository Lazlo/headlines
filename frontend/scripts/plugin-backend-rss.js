"use strict;"

var RssBackendPlugin_version = {"major": 0, "minor": 1};
var RssBackendPlugin_id = "rss";
var RssBackendPlugin_label = "RSS";
var RssBackendPlugin_capabilities = [];

class RssBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
