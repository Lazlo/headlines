"use strict;"

var VimeoBackendPlugin_version = {"major": 0, "minor": 1};
var VimeoBackendPlugin_id = "vimeo";
var VimeoBackendPlugin_label = "Vimeo";
var VimeoBackendPlugin_capabilities = [];

class VimeoBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
