"use strict;"

var JiraBackendPlugin_version = {"major": 0, "minor": 1};
var JiraBackendPlugin_id = "jira";
var JiraBackendPlugin_label = "Jira";
var JiraBackendPlugin_capabilities = ["issue-source"];

class JiraBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
