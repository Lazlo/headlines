"use strict;"

var AtomBackendPlugin_version = {"major": 0, "minor": 1};
var AtomBackendPlugin_id = "atom";
var AtomBackendPlugin_label = "Atom";
var AtomBackendPlugin_capabilities = [];

class AtomBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
