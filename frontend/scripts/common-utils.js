"use strict;"

function setElementVisible(e, visible) {
	if (visible) {
		e.style.display = "inline";
	} else {
		e.style.display = "none";
	}
}

function hideElement(e) {
	setElementVisible(e, false);
}

function showElement(e) {
	setElementVisible(e, true);
}

function isElementVisible(p) {
	return (p.style.display == "none") ? false : true;
}

function httpGet(url, responseTextHandler) {
	var xhr = new XMLHttpRequest();
	xhr.onreadystatechange = function(e) {
		if (xhr.readyState != 4)
			return;
		responseTextHandler(xhr.responseText);
	};
	xhr.open("GET", url, true);
	xhr.send();
}

function downloadTextAsFile(aTag, fileName, fileContents) {
	var header	= "data:text/plain:charset=utf-8";
	var payload	= encodeURIComponent(fileContents);
	var url		= header + "," + payload;

	aTag.download	= fileName;
	aTag.href	= url;
}

/* Argument datetime is expected to be of type Date */
function getTimePassedSinceNow(datetime) {
	diff = {
		'seconds': 0,
		'minutes': 0,
		'hours': 0,
		'days': 0
	};
	var ms_per_day = 1000 * 60 * 60 * 24;
	var now = new Date();

	var now_sec = now.getSeconds();
	var now_min = now.getMinutes();
	var now_hour = now.getHours();
	var now_days = (now.getTime() / ms_per_day).toFixed();
	var past_sec = datetime.getSeconds();
	var past_min = datetime.getMinutes();
	var past_hour = datetime.getHours();
	var past_days = (datetime.getTime() / ms_per_day).toFixed();

	if (past_sec > now_sec) {
		past_min = past_min - 1;
		now_sec = now_sec + 59;
	}
	diff['seconds'] = now_sec - past_sec;

	if (past_min > now_min) {
		past_hour = past_hour - 1;
		now_min = now_min + 59;
	}
	diff["minutes"] = now_min - past_min;

	if (past_hour > now_hour) {
		past_day = past_day - 1;
		now_hour = now_hour + 23;
	}
	diff["hours"] = now_hour - past_hour;

	diff["days"] = now_days - past_days;

	return diff;
}

function printTimePassedMostSignificantOnly(diff)
{
	if (diff["days"])
		return diff["days"] + " days";
	if (diff["hours"])
		return diff["hours"] + " hours";
	if (diff["minutes"])
		return diff["minutes"] + " minutes";
	return diff["seconds"];
}
