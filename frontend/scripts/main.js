"use strict;"

var pluginsToRegister = [
	GitlabBackendPlugin,
	GithubBackendPlugin,
	BitbucketBackendPlugin,
	TracBackendPlugin,
	RedmineBackendPlugin,
	JiraBackendPlugin,
	RequestTrackerBackendPlugin,
	MuninBackendPlugin,
	NagiosBackendPlugin,
	IcingaBackendPlugin,
	RsyslogBackendPlugin,
	GoogleAnalyticsBackendPlugin,
	FacebookBackendPlugin,
	YouTubeBackendPlugin,
	VimeoBackendPlugin,
	TwitterBackendPlugin,
	InstagramBackendPlugin,
	EMailBackendPlugin,
	SlackBackendPlugin,
	XmppBackendPlugin,
	IrcBackendPlugin,
	RssBackendPlugin,
	AtomBackendPlugin,
	ICalendarBackendPlugin,
	OdooBackendPlugin,
	WeclappBackendPlugin,
	SoundCloudBackendPlugin,
	MixcloudBackendPlugin,
	BandcampBackendPlugin,
	ProjectsPlugin,
	RepositoriesPlugin,
	IssuesPlugin,
	EventsPlugin,
	NewsPlugin,
	NotificationsPlugin,
	SlideshowPlugin,
];

var pluginRegistry = new PluginRegistry();
pluginRegistry.registerPlugins(pluginsToRegister);

var pluginProjects	= new ProjectsPlugin();
var pluginRepositories	= new RepositoriesPlugin();
var pluginIssues	= new IssuesPlugin();
var pluginEvents	= new EventsPlugin();
var pluginNews		= new NewsPlugin();
var pluginNotifications	= new NotificationsPlugin();
var slideshow		= new SlideshowPlugin();

function main() {

	/* This variable will later be part of the configuration. Users will be able to configure
	 * a list of widgets that should appear in the slideshow.
	 *
	 * For now we manually construct this dictionary. */
	var widgets = [
		{"tagId": "plugin-projects",		"tag": null, "pluginObj": pluginProjects},
		{"tagId": "plugin-repos",		"tag": null, "pluginObj": pluginRepositories},
		{"tagId": "plugin-issues",		"tag": null, "pluginObj": pluginIssues},
		{"tagId": "plugin-events",		"tag": null, "pluginObj": pluginEvents},
		{"tagId": "plugin-news",		"tag": null, "pluginObj": pluginNews},
		{"tagId": "plugin-notifications",	"tag": null, "pluginObj": pluginNotifications},

	];
	var i;
	for (i = 0; i < widgets.length; i++) {
		widgets[i]["tag"] = document.getElementById(widgets[i]["tagId"]);
	}

	var slideDelaySec = 5;
	var repeat = true;
	slideshow.setup(widgets, slideDelaySec, repeat);
	slideshow.restoreConfigs();

	/* Only after the settings object in the slideshow is ready,
	 * we can pass a instance of the settings object form within the
	 * slideshow object to other plugins that make use of existing configurations
	 * managed by the settings object. */

	pluginProjects.setupSettings(slideshow.settings);
	pluginIssues.setupSettings(slideshow.settings);

	slideshow.run();
}
