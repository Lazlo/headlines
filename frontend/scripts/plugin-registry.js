"use strict;"

class PluginRegistry {

	constructor() {
		this.debug = false;
		this._pluginList = [];
	}

	_log(msg) {
		if (!this.debug)
			return;
		var prefix = "PluginRegistry.";
		console.log(prefix + msg);
	}

	_isRegistered(plugin) {
		this._log("isRegistered()");
		var i;
		for (i = 0; i < this._pluginList.length; i++) {
			if (plugin == this._pluginList[i])
				return true;
		}
		return false;
	}

	register(plugin) {
		this._log("register()");
		if (this._isRegistered(plugin))
			return;
		/*
		var logMsg = "registering \"" + plugin.name + "\" v" + plugin.getVersion();
		console.log(logMsg);
		*/
		this._pluginList.push(plugin);
	}

	registerPlugins(list) {
		var i;
		for (i = 0; i < list.length; i++) {
			this.register(list[i]);
		}
	}

	getPluginById(pluginId) {
		this._log("getPluginById()");
		var list = this._pluginList;
		var i;
		for (i = 0; i < list.length; i++) {
			var classObj = list[i];
			if (pluginId != classObj.getId())
				continue;
			return classObj;
		}
	}

	getPluginsEndingWith(str) {
		var list = this._pluginList;
		var i;
		var res = [];
		for (i = 0; i < list.length; i++) {
			if (!list[i].name.endsWith(str))
				continue;
			res.push(list[i]);
		}
		return res;
	}

	getPluginsWithCapability(str) {
		var list = this._pluginList;
		var i;
		var res = [];
		for (i = 0; i < list.length; i++) {
			var classObj = list[i];
			var caps = classObj.getCapabilities();
			if (!caps) {
				continue;
			}
			if (caps.includes(str) == false) {
				continue;
			}
			res.push(list[i]);
		}
		return res;
	}

	_sortPluginListByName(pluginList) {
		var orderedList = []

		/* Build a list of plugins labels and a list of plugin objects */

		var pluginClassList = [];
		var pluginLabelList = [];

		var list = pluginList;
		var i;
		for (i = 0; i < list.length; i++) {
			var classObj = list[i];
			var pluginLabelLowerCased = classObj.getLabel().toLowerCase();

			pluginClassList.push(classObj);
			pluginLabelList.push(pluginLabelLowerCased);
		}

		/* Sort the list of lower cased plugin labels */

		pluginLabelList.sort();

		/* Build a list with plugin objects, orderd by name */

		var list = pluginLabelList;
		var i;
		for (i = 0; i < list.length; i++) {
			var pluginLabelLowerCased = list[i];

			/* Find the plugin object that matches the label from the sorted list */

			var n;
			for (n = 0; n < pluginClassList.length; n++) {
				var classObj = pluginClassList[n];

				if (pluginLabelLowerCased != classObj.getLabel().toLowerCase())
					continue;
				break;
			}
			orderedList.push(classObj);
		}

		return orderedList;
	}
}
