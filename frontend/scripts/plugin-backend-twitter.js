"use strict;"

var TwitterBackendPlugin_version = {"major": 0, "minor": 1};
var TwitterBackendPlugin_id = "twitter";
var TwitterBackendPlugin_label = "Twitter";
var TwitterBackendPlugin_capabilities = [];

class TwitterBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
