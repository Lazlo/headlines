"use strict;"

var IrcBackendPlugin_version = {"major": 0, "minor": 1};
var IrcBackendPlugin_id = "irc";
var IrcBackendPlugin_label = "IRC";
var IrcBackendPlugin_capabilities = [];

class IrcBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
