"use strict;"

var NotificationsPlugin_version = {"major": 0, "minor": 1};
var NotificationsPlugin_id = "notifications";
var NotificationsPlugin_label = "Notifications";
var NotificationsPlugin_capabilities = [];

class NotificationsPlugin extends BasePlugin {

	constructor() {
		super();
		this.log("NotificationsPlugin");
	}
}
