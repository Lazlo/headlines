"use strict;"

var MixcloudBackendPlugin_version = {"major": 0, "minor": 1};
var MixcloudBackendPlugin_id = "mixcloud";
var MixcloudBackendPlugin_label = "Mixcloud";
var MixcloudBackendPlugin_capabilities = [];

class MixcloudBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
