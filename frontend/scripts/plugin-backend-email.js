"use strict;"

var EMailBackendPlugin_version = {"major": 0, "minor": 1};
var EMailBackendPlugin_id = "email";
var EMailBackendPlugin_label = "E-Mail";
var EMailBackendPlugin_capabilities = [];

class EMailBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
