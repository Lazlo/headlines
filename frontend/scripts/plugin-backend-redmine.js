"use strict;"

var RedmineBackendPlugin_version = {"major": 0, "minor": 1};
var RedmineBackendPlugin_id = "redmine";
var RedmineBackendPlugin_label = "Redmine";
var RedmineBackendPlugin_capabilities = ["repository-source", "issue-source"];

class RedmineBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
