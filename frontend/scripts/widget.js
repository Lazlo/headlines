"use strict;"

function getPluginWidgetsAvailable() {
	var className = "plugin-widget";
	var pluginList = document.getElementsByClassName(className);
	return pluginList;
}

function hideAllPluginWidgets(list) {
	var i;
	for (i = 0; i < list.length; i++) {
		var p = list[i];
		hideElement(p);
	}
}
