"use strict;"

var GitlabBackendPlugin_version = {"major": 0, "minor": 1};
var GitlabBackendPlugin_id = "gitlab";
var GitlabBackendPlugin_label = "GitLab";
var GitlabBackendPlugin_capabilities = ["repository-source", "isssue-source"];

class GitlabBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();

		this.privateToken = null;
		this.settings = new GitlabBackendPluginSettings();
	}

	setPrivateToken(privateToken) {
		this.log("pluginBackendGitlab.setPrivateToken()");
		this.privateToken = privateToken;
	}

	fetchProjects(responseTextHandler) {
		this.log("pluginBackendGitlab.fetchProjects()");
		var url;

		url = this.settings.defaultApiBaseUrl;
		url += "/" + "projects";
		url += "?" + "owned=true";
		if (this.privateToken)
			url += "&" + "private_token=" + this.privateToken;
		this.log(url);
		httpGet(url, responseTextHandler);
	}

	fetchIssues(responseTextHandler) {
		this.log("pluginBackendGitlab.fetchIssues()");
		var url;

		url = this.settings.defaultApiBaseUrl;
		url += "/" + "issues";
		if (this.privateToken)
			url += "?" + "private_token=" + this.privateToken;
		this.log(url);
		httpGet(url, responseTextHandler);
	}
}

///////////////////////////////////////////////////////////////////////

class GitlabBackendPluginSettings {

	constructor() {
		//this.debug = true;
		this.schema = [];
		this.schema.push({"settingsKey": "name",		"tagId": "slideshow-backend-config-gitlab-name"});
		this.schema.push({"settingsKey": "url",			"tagId": "slideshow-backend-config-gitlab-url"});
		this.schema.push({"settingsKey": "privateToken",	"tagId": "slideshow-backend-config-gitlab-private-token"});
		this.schema.push({"settingsKey": "user",		"tagId": "slideshow-backend-config-gitlab-user"});
		this.schema.push({"settingsKey": "passwd",		"tagId": "slideshow-backend-config-gitlab-passwd"});
		this.schema.push({"settingsKey": "httpAuthUser",	"tagId": "slideshow-backend-config-gitlab-http-auth-user"});
		this.schema.push({"settingsKey": "httpAuthPasswd",	"tagId": "slideshow-backend-config-gitlab-http-auth-passwd"});

		/* These radio buttons only control what form fields
		 * will be visible to the user. */

		this.hostingRadioTagIds = [
			"slideshow-backend-config-gitlab-hosting-com",
			"slideshow-backend-config-gitlab-hosting-self",
		];

		this.defaultApiBaseUrl =  "https://gitlab.com/api/v4";

		this._hideOptFormFields();
	}

	_log(msg) {
		if (!this.debug)
			return;
		var prefix = "GitlabBackendPluginSettings.";
		console.log(prefix + msg);
	}

	getSchema() {
		this._log("getSchema()");
		return this.schema;
	}

	_setFormSectionVisibleByInputTagId(inputTagId, visible) {
		this._log("_setFormSectionVisible()");
		var sectionTagId = inputTagId + "-section";
		var input = document.getElementById(sectionTagId);
		var displayValue;
		if (!visible)
			displayValue = "none";
		else
			displayValue = "block";
		input.style.display = displayValue;
	}

	_setFormSectionVisibleBySettingsKey(settingsKey, visible) {
		var list = this.schema;
		var i;
		for (i = 0; i < list.length; i++) {
			if (list[i]["settingsKey"] != settingsKey)
				continue;
			var inputTagId = list[i]["tagId"];
			this._setFormSectionVisibleByInputTagId(inputTagId, visible);
			return;
		}
	}

	_hideOptFormFields() {
		this._log("hideOptFormFields()");
		/* Hide url, user, passwd and http auth options by default */
		var hidden = ["url", "user", "passwd", "httpAuthUser", "httpAuthPasswd"];
		var list = hidden;
		var i;
		for (i = 0; i < list.length; i++) {
			var settingsKey = list[i];
			this._setFormSectionVisibleBySettingsKey(settingsKey, false);
		}
	}

	_onHostingRadioChange() {
		this._log("_onHostingRadioChange()");
		var list = this.hostingRadioTagIds;
		var i;
		for (i = 0; i < list.length; i++) {
			var tagId = list[i];
			var radio = document.getElementById(tagId);
			if (!radio.checked)
				continue;
			var value = radio.value;
			var show = value == "com" ? false : true;
			this._setFormSectionVisibleBySettingsKey("url", show);
			this._setFormSectionVisibleBySettingsKey("httpAuthUser", show);
			this._setFormSectionVisibleBySettingsKey("httpAuthPasswd", show);
		}
	}

	addEventListenersToForm() {
		this._log("addEventListenersToForm()");
		var list = this.hostingRadioTagIds;
		var i;
		for (i = 0; i < list.length; i++) {
			var tagId = list[i];
			var radio = document.getElementById(tagId);
			radio.onchange = this._onHostingRadioChange.bind(this);
		}
	}

	resetFormFields() {
		this._log("resetFormFields()");
		var list = this.schema;
		var i;
		for (i = 0; i < list.length; i++) {
			var settingsKey = list[i]["settingsKey"];
			var tagId = list[i]["tagId"];
			var input = document.getElementById(tagId);
			input.value = null;
			if (settingsKey == "url") {
				input.value = this.defaultApiBaseUrl;
			}
		}
		/* Reset the hosting radio button group to "com" */
		var list = this.hostingRadioTagIds;
		for (i = 0; i < list.length; i++) {
			var tagId = list[i];
			var input = document.getElementById(tagId);
			input.checked = (input.value == "com") ? true : false;
		}
	}

	createConfigFromFormFields() {
		this._log("createConfigFromFormFields()");
		var config = {};
		var i;
		for (i = 0; i < this.schema.length; i++) {
			var settingsKey = this.schema[i]["settingsKey"];
			var tagId = this.schema[i]["tagId"];

			/* Start by adding the key to the dict */
			config[settingsKey] = null;

			/* Get the value from the tag */
			var input = document.getElementById(tagId);

			/* Save the value in the dict */
			config[settingsKey] = input.value;
		}
		return config;
	}
}
