"use strict;"

var WeclappBackendPlugin_version = {"major": 0, "minor": 1};
var WeclappBackendPlugin_id = "weclapp";
var WeclappBackendPlugin_label = "Weclapp";
var WeclappBackendPlugin_capabilities = [];

class WeclappBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
