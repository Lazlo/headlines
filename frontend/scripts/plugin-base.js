"use strict;"

var BasePlugin_version = {"major": null, "minor": null};
var BasePlugin_id = null;
var BasePlugin_label = null;
var BasePlugin_capabilities = [];

class BasePlugin {

	constructor() {
		this.debug = false;
	}

	log(msg) {
		if (!this.debug)
			return;
		console.log(msg);
	}

	static getVersion() {
		var version = eval(this.name + "_version");
		var versionStr = version["major"] + "." + version["minor"];
		return versionStr;
	}

	static getId() {
		var id = eval(this.name + "_id");
		return id;
	}

	static getLabel() {
		var label = eval(this.name + "_label");
		return label;
	}

	static getCapabilities() {
		var capabilities = null;
		try {
			capabilities = eval(this.name + "_capabilities");
		} catch (err) {
			console.warn("Faied getting capabilities for " + this.name);
		}
		return capabilities;
	}
}
