"use strict;"

var BandcampBackendPlugin_version = {"major": 0, "minor": 1};
var BandcampBackendPlugin_id = "bandcamp";
var BandcampBackendPlugin_label = "Bandcamp";
var BandcampBackendPlugin_capabilities = [];

class BandcampBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
