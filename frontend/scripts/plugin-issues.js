"use strict;"

var IssuesPlugin_version = {"major": 0, "minor": 1};
var IssuesPlugin_id = "issues";
var IssuesPlugin_label = "Issues";
var IssuesPlugin_capabilities = [];

class IssuesPlugin extends BasePlugin {

	constructor() {
		super();
		//this.debug = true;

		this.log("IssuesPlugin()");

		this.refToSettingsPluginObj = null;
		/* FIXME The item name should reflect the plugin instance */
		this.localStorageSettingsItemKey = "plugin-issues-1-settings";
		this.settings = {
			'backendConfigId': null,
		};

		this.ui = {
			'issuesList'			: {"tagId": null, "tag": null},
			'issuesListTimestamp'		: {"tagId": null, "tag": null},
			'supportedBackends'		: {"tagId": null, "tag": null},
			'settingsBackendConfig'		: {"tagId": null, "tag": null},
		};
		this.ui.issuesList.tagId		= "plugin-issues-list";
		this.ui.issuesListTimestamp.tagId	= "issue-list-last-updated";
		this.ui.supportedBackends.tagId		= "plugin-issues-supported-backends";
		this.ui.settingsBackendConfig.tagId	= "plugin-issues-backend-config";
		this.ui.issuesList.tag			= document.getElementById(this.ui.issuesList.tagId);
		this.ui.issuesListTimestamp.tag		= document.getElementById(this.ui.issuesListTimestamp.tagId);
		this.ui.supportedBackends.tag		= document.getElementById(this.ui.supportedBackends.tagId);
		this.ui.settingsBackendConfig.tag	= document.getElementById(this.ui.settingsBackendConfig.tagId);
	}

	_log(msg) {
		if (!this.debug)
			return;
		var prefix = "IssuesPlugin.";
		console.log(prefix + msg);
	}

	/* NOTE This method is identical to the one in ProjectsPlugin.
	 * Think about moving the method to a common place. */
	_restoreSettingsFromLocalStorage() {
		this._log("restoreSettingsFromLocalStorage()");
		var itemKey = this.localStorageSettingsItemKey;
		var c = new Configuration();
		var itemValue = c.load(itemKey);
		if (!itemValue)
			return;
		this.settings = itemValue;
	}

	/* NOTE This method is identical to the one in ProjectsPlugin.
	 * Think about moving the method to a common place. */
	_saveSettingsToLocalStorage() {
		this._log("saveSettingsToLocalStorage()");
		var itemKey = this.localStorageSettingsItemKey;
		var c = new Configuration();
		c.save(itemKey, this.settings);
	}

	/* NOTE This method is identical to the one in ProjectsPlugin.
	 * Think about moving the method to a common place. */
	_onSelectedBackendConfigChanged() {
		this._log("_onSelectedBackendConfigChanged()");

		var select = this.ui.settingsBackendConfig.tag;
		var selected = select.options[select.selectedIndex].value;
		var configId = selected.replace("config-id-", "");
		this.settings.backendConfigId = configId;
	}

	/* NOTE This method is identical to the one in ProjectsPlugin.
	 * Think about moving the method to a common place. */
	setupSettings(settingsPluginObj) {
		this._log("setupSettings()");

		this._restoreSettingsFromLocalStorage();

		var select = this.ui.settingsBackendConfig.tag;

		this.refToSettingsPluginObj = settingsPluginObj;

		select.onchange = this._onSelectedBackendConfigChanged.bind(this);

		var opt = document.createElement("option");
		select.innerHTML = null;
		select.appendChild(opt);

		var list = settingsPluginObj.configuredBackends;
		var i;
		for (i = 0; i < list.length; i++) {
			var opt = document.createElement("option");
			var att = document.createAttribute("value");
			att.value = "config-id-" + list[i]["id"];
			opt.setAttributeNode(att);
			opt.innerHTML = "[" + list[i]["backend"] + "] " + list[i]["name"];
			if (this.settings.backendConfigId == list[i]["id"]) {
				opt.selected = true;
			}
			select.appendChild(opt);
		}

		/* Populate the list of supported backends */
		var target = this.ui.supportedBackends.tag;
		target.innerHTML = "";
		/* FIXME Accessing global variables is evil! */
		var list = pluginRegistry.getPluginsWithCapability("issue-source")
		for (i = 0; i < list.length; i++) {
			target.innerHTML += list[i].getLabel();
			if (i + 1 < list.length)
				target.innerHTML += ", ";
		}

		this.update();
	}

	saveSettings() {
		this._log("saveSettings()");
		this._saveSettingsToLocalStorage();
	}

	update() {
		this._log("update()");
		var issuesList = this.ui.issuesList.tag;

		if (this.settings.backendConfigId == null) {
			var errMsg = "The plugin is unconfigured.";
			console.log("IssuesPlugin: " + errMsg);
			issuesList.innerHTML = "<p>" + errMsg + "</p>";
			return;
		}

		var configId = this.settings["backendConfigId"];
		var config = this.refToSettingsPluginObj._getConfiguredBackendByConfigId(configId);

		if (!config) {
			console.warn("Failed to get config with id " + configId);
			return;
		}

		var b = new GitlabBackendPlugin();
		//b.debug = true;
		b.setPrivateToken(config["privateToken"]);
		b.fetchIssues(this.populateIssuesList.bind(this));
	}

	_createIssueListElement(projectId, issueId, issueLabel) {
		this._log("pluginIssues._createIssueListElement()");
		var elem = document.createElement("li");
		elem.innerHTML = "[" + projectId + "]" + " " + "#" + issueId + " - " + issueLabel;
		return elem;
	}

	populateIssuesList(responseText) {
		this._log("pluginIssues.populateIssuesList()");
		var targetTag = this.ui.issuesList.tag;

		var jsonResponse = JSON.parse(responseText);

		/* TODO Get hold of the private token.
		 *
		 * This requires having a settings dialog where one can select
		 * the backend configuration to use.
		 */
		/* Reset content */
		targetTag.innerHTML = null;
		/* Add issues */
		var list = jsonResponse;
		var i;
		for (i = 0; i < list.length; i++) {
			var issueData = list[i];
			var issueElem = this._createIssueListElement(issueData.project_id, issueData.iid, issueData.title);
			targetTag.appendChild(issueElem);
		}

		/* Update the 'last updated' time stamp */
		var target = this.ui.issuesListTimestamp.tag;
		target.innerHTML = new Date();
	}
}
