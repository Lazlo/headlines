"use strict;"

var EventsPlugin_version = {"major": 0, "minor": 1};
var EventsPlugin_id = "events";
var EventsPlugin_label = "Events";
var EventsPlugin_capabilities = [];

class EventsPlugin extends BasePlugin {

	constructor() {
		super();
		this.log("EventsPlugin()");
	}
}
