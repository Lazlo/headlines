"use strict;"

var NagiosBackendPlugin_version = {"major": 0, "minor": 1};
var NagiosBackendPlugin_id = "nagios";
var NagiosBackendPlugin_label = "nagios";
var NagiosBackendPlugin_capabilities = [];

class NagiosBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
