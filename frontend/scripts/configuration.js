"use strict;"

class Configuration {

	constructor() {
		//this.debug = true;
	}

	_log(msg) {
		if (!this.debug)
			return;
		var prefix = "Configuration.";
		console.log(prefix + msg);
	}

	load(itemKey) {
		this._log("load()");
		var itemValue = null;
		try {
			var store = window.localStorage;
			var itemValueJson = store.getItem(itemKey);
			if (itemValueJson) {
				itemValue = JSON.parse(itemValueJson);
			}
		} catch(err) {
			console.error(err);
		}
		return itemValue;
	}

	save(itemKey, itemValue) {
		this._log("save()");
		var itemValueJson = JSON.stringify(itemValue);
		try {
			var store = window.localStorage;
			store.setItem(itemKey, itemValueJson);
		} catch(err) {
			console.error(err);
		}
	}

	download() {
		this._log("download()");
	}
}
