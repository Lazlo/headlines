"use strict;"

var YouTubeBackendPlugin_version = {"major": 0, "minor": 1};
var YouTubeBackendPlugin_id = "youtube";
var YouTubeBackendPlugin_label = "YouTube";
var YouTubeBackendPlugin_capabilities = [];

class YouTubeBackendPlugin extends BaseBackendPlugin {

	constructor() {
		super();
	}
}
