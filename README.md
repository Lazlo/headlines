# Headlines

Is a web-based dashboard that will display information from various backends. It is implemented using JavaScript, HTML and CSS.

## Usage

To run the dashboard, no HTTP server is required. Simply open the frontend/index.html in your browser.

After opening the frontend you will first have to configure backends that will be used to acquire data.

The configuration created will be stored in the browser and will persist over closing and re-opening the browser.
